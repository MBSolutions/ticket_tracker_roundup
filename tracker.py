import xmlrpclib
from trytond.modules.ticket_tracker import TrackerHandler
from urlparse import urlunparse, urlparse
from trytond.model import ModelSQL, ModelView
import copy
from trytond.pool import Pool
from datetime import datetime


_ROUNDUP_STATUS_MAP = {
    #roundup-status: tryton-states
    'new': 'open',
    'chatting': 'open',
    'need-eg': 'open',
    'waiting-approval': 'open',
    'approved': 'open',
    'in-progress': 'open',
    'testing': 'open',
    'resolved': 'closed',
}

# Roundup Date Format from XMLRPC <Date 2011-09-02.08:11:27.559>
_ROUNDUP_DATE_FORMAT = '<Date %Y-%m-%d.%H:%M:%S.%f>'


class Tracker(ModelSQL, ModelView):
    _name = 'ticket.tracker'

    def __init__(self):
        super(Tracker, self).__init__()
        self.type = copy.copy(self.type)
        if ('roundup', 'Roundup') not in self.type.selection:
            self.type.selection += [('roundup', 'Roundup')]
        self._reset_columns()

    def _check_update(self, tracker_ticket, type):
        res = super(Tracker, self)._check_update(tracker_ticket, type)
        if type == 'roundup':
            res = self._check_update_roundup(tracker_ticket)
        return res

    def _check_update_roundup(self, roundup_ticket):
        ticket_obj = Pool().get('ticket.work')

        res = True
        # Match roundup ticket and tryton ticket, if exists.
        tryton_ticket = ticket_obj.search_read([
            ('tracker_code', '=', roundup_ticket['id']),
        ], limit=1, fields_names=['tracker_last_change', 'last_sync_state'])
        # Roundup Date Format from XMLRPC <Date 2011-09-02.08:11:27.559>
        last_change = datetime.strptime(roundup_ticket.get('activity'),
            _ROUNDUP_DATE_FORMAT).replace(microsecond=0)

        if (tryton_ticket and tryton_ticket['tracker_last_change'] and
                tryton_ticket['tracker_last_change'] >= last_change):
            res = False
        return res

    def map_tryton_ticket(self, data, type):
        if type == 'roundup':
            tryton_ticket_id, tryton_ticket_data, message, sync_state, \
                tryton_ticket_state = self._map_tryton_ticket_roundup(data)
            tryton_ticket_data, message, tryton_ticket_state, sync_state = \
                self._check_state_roundup(tryton_ticket_data, message,
                    tryton_ticket_state, sync_state)
            return (tryton_ticket_id, tryton_ticket_data, message.strip(),
                sync_state)
        return super(Tracker, self).map_tryton_ticket(data, type)

    def _map_tryton_ticket_roundup(self, roundup_ticket):
        employee_obj = Pool().get('company.employee')
        ticket_obj = Pool().get('ticket.work')

        tryton_ticket_data = {}
        message = ''
        sync_state = 'ok'
        # General map roundup to tryton
        for _map in self.ticket2tryton():
            tryton_ticket_data[_map[1]] = roundup_ticket.get(_map[0]) or _map[2]

        # Map roundup status to tryton status
        if roundup_ticket.get('status'):
            tryton_ticket_state = _ROUNDUP_STATUS_MAP.get(
                roundup_ticket['status'], 'open')
            tryton_ticket_data['state'] = tryton_ticket_state

        if roundup_ticket.get('activity'):
            # Roundup Date Format from XMLRPC <Date 2011-09-02.08:11:27.559>
            tryton_ticket_data['tracker_last_change'] = datetime.strptime(
                roundup_ticket.get('activity'), _ROUNDUP_DATE_FORMAT)
        # Map roundup assignedto to tryton user
        assigned_to = None
        if roundup_ticket.get('assignedto'):
            employee = employee_obj.search([
                ('roundup_user', '=', roundup_ticket['assignedto']),
                ])
            if employee:
                if isinstance(employee, dict):
                    employee = [employee]
                if len(employee) == 1:
                    assigned_to = employee[0]
        if not assigned_to:
            # Can not close the ticket, when there is no owner
            message += ('MATCH ERROR: Can not find a Tryton employee '
                'for Roundup Assinged To "%s"\n' % (
                roundup_ticket['assignedto'],))
            sync_state = 'not_ok'
            tryton_ticket_state = 'open'
        tryton_ticket_data['assigned_to'] = assigned_to

        requestor_id, message = self._map_requestor_roundup(roundup_ticket,
            message)
        if not requestor_id:
            sync_state = 'not_ok'
            tryton_ticket_state = 'open'
        tryton_ticket_data['requestor'] = requestor_id

        # Match roundup ticket and tryton ticket, if exists.
        tryton_ticket_id = ticket_obj.search([
            ('tracker_code', '=', roundup_ticket['id']),
            ], limit=1)
        if tryton_ticket_id and isinstance(tryton_ticket_id, list):
            tryton_ticket_id = tryton_ticket_id[0]

        return (tryton_ticket_id or None, tryton_ticket_data, message,
                sync_state, tryton_ticket_state)

    def _map_requestor_roundup(self, roundup_ticket, message):
        contact_mechanism_obj = Pool().get('party.contact_mechanism')
        party_obj = Pool().get('party.party')

        requestor_id = None
        roundup_requestor = roundup_ticket.get('creator')
        if not roundup_requestor:
            message += ('MATCH ERROR: No Roundup requestor set on this '
                'ticket!\n')
        # Step1: Check for a party with the email address:
        contact_values = contact_mechanism_obj.search_read([
            ('value', 'ilike', roundup_requestor),
            ('type', '=', 'email'),
            ], fields_names=['party'])
        if contact_values:
            if len(contact_values) == 1:
                requestor_id = contact_values[0].get('party')
            else:
                message += ('MATCH ERROR: Multiple parties match Roundup '
                    'requestor! Parties need to have unique email '
                    'addresses for automated assignment.\n')
        # Step2: Check for an organization with the email domain
        _, requestor_email_domain = roundup_requestor.split('@')
        organization_ids = party_obj.search([
            ('party_type', '=', 'organization')])
        contact_values = contact_mechanism_obj.search_read([
            ('value', 'ilike', '%' + requestor_email_domain),
            ('type', '=', 'email'),
            ('party', 'in', organization_ids)
            ], fields_names=['party'])
        if contact_values:
            if len(contact_values) == 1:
                requestor_id = contact_values[0].get('party')
                requestor = party_obj.browse(requestor_id)
                message += ('MATCH INFO: Fuzzy match Roundup requestor %s '
                    'to organization "%s" with code "%s"\n' % (
                        roundup_requestor, requestor.full_name, requestor.code))
            else:
                message += ('MATCH ERROR: Multiple organizations match '
                    'Roundup requestor email domain "%s"! '
                    'Organizations need to have unique email '
                    'domains for automated assignment.\n' %
                    (requestor_email_domain,))
        else:
            message += ('MATCH ERROR: Can not match Roundup requestor %s\n' % (
                roundup_requestor,))
        return requestor_id, message

    def ticket2tryton(self):
        return [
            # roundup-variable, tryton-variable, default value
            ('id', 'tracker_code', None),
            ('title', 'name', '--'),
            ('project', 'project', None),
            ]

    def _check_state_roundup(self, tryton_ticket_data, message,
            tryton_ticket_state, sync_state):
        if tryton_ticket_data['state'] != tryton_ticket_state:
            message += ('Can not set Tryton ticket state to '
                '"%s" because of previous problems.\n' % (
                    tryton_ticket_data['state'],))
            sync_state = 'not_ok'
            tryton_ticket_data['state'] = tryton_ticket_state
        return (tryton_ticket_data, message, tryton_ticket_state, sync_state)

Tracker()


class RoundupHandler(TrackerHandler):
    'Roundup Tracker Handler'
    _name = 'roundup'

    def __init__(self, tracker):
        super(RoundupHandler, self).__init__()
        self.config = tracker
        self.login = tracker.login
        self.password = tracker.password
        self.url = tracker.url
        parsed = urlparse(self.url)
        self.host = parsed.hostname
        self.port = parsed.port
        self.protocol = parsed.scheme
        new_netloc = self.login + ':' + self.password + '@' + self.host
        if self.port:
            new_netloc += ':' + str(self.port)
        new_path = parsed.path
        if not new_path.endswith('/'):
            new_path += '/'
        url = (parsed[0], new_netloc,) + parsed[2:]
        self.real_url = urlunparse(url)
        self.connection = None

    def _connect(self):
        if not self.connection:
            res = xmlrpclib.ServerProxy(self.real_url, allow_none=True)
            self.connection = res
        return res

    def check_connection(self):
        if not self.connection:
            self._connect()
        try:
            self.connection.list('user')
        except:
            return False
        return True

    def get_tickets(self, ticket_ids=None):
        '''
        Get values of roundup tickets

        param ticket_ids: a list of ids of the tickets in the roundup tracker
        return: a list of dictionaries containing the ticket values
        '''
        user_ids = self.connection.list('user', 'id')
        id2username = {}
        id2email = {}
        for user_id in user_ids:
            user = self.connection.display('user' + user_id, 'username',
                'address')
            id2username[user_id] = user['username']
            id2email[user_id] = user['address']

        state_ids = self.connection.list('status', 'id')
        id2state = {}
        for state_id in state_ids:
            id2state[state_id] = self.connection.display('status' + state_id,
                'name')['name']

        organisation_ids = self.connection.list('organisation', 'id')
        id2organisation = {}
        for organisation_id in organisation_ids:
            id2organisation[organisation_id] = self.connection.display(
                'organisation' + organisation_id, 'code')['code']

        if not ticket_ids:
            ticket_ids = self.connection.list('issue', 'id')

        # TODO: Use Dictionary with id as key instead of list
        tickets = []
        for ticket_id in ticket_ids:
            ticket = self.connection.display('issue' + ticket_id, 'creator',
                'status', 'activity', 'id', 'organisations', 'assignedto',
                'title', 'project')
            if ticket.get('assignedto'):
                ticket.update({'assignedto': id2username[ticket['assignedto']]})
            if ticket.get('creator'):
                ticket.update({'creator': id2email[ticket['creator']]})
            if ticket.get('organisations'):
                org_codes = []
                for orgid in ticket['organisations']:
                    org_codes.append(id2organisation[orgid])
                ticket.update({'organisations': org_codes})
            ticket.update({'status': id2state[ticket['status']]})
            tickets.append(ticket)
        return tickets

    def get_backlink(self, code):
        callback_url = ''
        if code:
            callback_url = "%s/issue%s" % (
                self.url.rstrip('/'), code)
        return callback_url
